//////////////////////////////////////////////
// 設定變數
//////////////////////////////////////////////
// 動畫效果的速度
var effectSpeed = null;
// 動畫效果的種類
var effectType = null;
// 前一個動畫效果的種類
var prevEffectType = null;
// 動畫效果的特定選項
var effectOptions = {}; 
// 動畫效果的最初速度
var orgEffectSpeed = null;
// 動畫效果的最初種類
var orgEffectType = null;
// 動畫效果的最初特定選項
var orgEffectOptions = {}; 
//////////////////////////////////////////////
// 顯示各種資料形態的數值
//////////////////////////////////////////////
function displayData(obj) {
	if (obj == null) {
		return 'null';
	}
	switch (typeof obj) {
		case 'number':
		case 'boolean':
		case 'function':
			return obj;
			break;
		// 顯示 『動畫效果的速度』與 『動畫效果的種類』數值
		case 'string':
			return '\'' + obj + '\'';
			break;
		// 顯示 『動畫效果的特定選項』的數值
		case 'object':
			var result;
			// 日期
			if (obj instanceof Date) {
				result = 'new Date(' + obj.getTime() + ')';
			}
			// 陣列
			else if (obj.constructor === Array || typeof obj.callee !== 'undefined') {
				result = '[';
				var i, length = obj.length;
				for (i = 0; i < length - 1; i++) {
					result += displayData(obj[i]) + ',';
				}
				result += displayData(obj[i]) + ']';
			}
			else {
				result = '{';
				var key;
				for (key in obj) {
					result += key + ':' + displayData(obj[key]) + ',';
				}
				result = result.replace(/\,$/, '') + '}';
			}
			return result;
			break;
		default:
			return '?unsupported-type?';
			break;
	}
};
//////////////////////////////////////////////
// 取得 『動畫效果的速度』 核取按鈕的選項
//////////////////////////////////////////////
function getEffectSpeed() {
	// 讀取動畫效果的速度字串
	effectSpeed = $('[name="radioEffectSpeed"]:checked').val();
	// 動畫效果的速度是自訂的毫秒數
	if (effectSpeed == '自訂') {
		// 讀取自訂的毫秒數
		effectSpeed = parseInt($('#miliseconds').val());
		// 毫秒數的數值不正確
		if (isNaN(effectSpeed) || (effectSpeed < 0)) {
			effectSpeed = 'normal';
		}
	}
	// 不設定動畫效果的速度
	if (effectSpeed == '無') {
		effectSpeed = null;
	}
};
//////////////////////////////////////////////
// 取得 『動畫效果的種類』 核取按鈕的選項
//////////////////////////////////////////////
function getEffectType() {
	// 前一個選取元素的CSS類別名稱
	var elementCSS = '.' + prevEffectType;
	// 隱藏指定CSS類別名稱的元素, 移除前一個 『動畫效果的特定選項』
	$(elementCSS).removeClass('show');
	// 讀取動畫效果的種類
	effectType = $('[name="radioEffectType"]:checked').val();
	// 取得動畫效果的特定選項
	getEffectOptions();
	// 前一個動畫效果的種類
	prevEffectType = effectType;
};
//////////////////////////////////////////////
// 取得 『動畫效果的特定選項』
//////////////////////////////////////////////
function getEffectOptions() {
	// blind, clip
	if (effectType == "blind" || effectType == "clip") {
		effectOptions = {
			direction: $('[name="direction"]:checked').val(), 
		  mode: $('[name="mode"]:checked').val()};
	// bounce
	} else if (effectType == "bounce") {
		effectOptions = {
			direction: $('[name="direction4"]:checked').val(), 
		  distance: parseInt($('[name="distance"]').val()), 
			mode: $('[name="mode3"]:checked').val(), 
			times: parseInt($('[name="times"]').val())};
	// drop
	} else if (effectType == "drop") {
		effectOptions = {
			direction: $('[name="direction4"]:checked').val(), 
			mode: $('[name="mode"]:checked').val()};
	// explode
	} else if (effectType == "explode") {
		effectOptions = {
			mode: $('[name="mode"]:checked').val(), 
			pieces: parseInt($('[name="pieces"]').val())};
	// fade
	} else if (effectType == "fade") {
		effectOptions = {};	
	// fold
	} else if (effectType == "fold") {
		effectOptions = {
			horizFirst: eval($('[name="horizFirst"]:checked').val()), 
			mode: $('[name="mode"]:checked').val(), 
			size: parseInt($('[name="size"]').val())};
	// highlight
	} else if (effectType == "highlight") {
		effectOptions = {
			color: $('[name="color"]').val(), 
			mode: $('[name="mode"]:checked').val()};
	// pulsate
	} else if (effectType == "pulsate") {
		effectOptions = {
			mode: $('[name="mode"]:checked').val(), 
			times: parseInt($('[name="times"]').val())};
	// puff
	} else if (effectType == "puff") {
		effectOptions = {
			mode: $('[name="mode"]:checked').val(),
			percent: parseFloat($('[name="percent"]').val())};
	// scale
	} else if (effectType == "scale") {
		effectOptions = {
			direction: $('[name="direction3"]:checked').val(),
			mode: $('[name="mode4"]:checked').val(),
			percent: parseFloat($('[name="percent"]').val()), 
			scale: $('[name="scale"]:checked').val()};
	// shake
	} else if (effectType == "shake") {
		effectOptions = {
			direction: $('[name="direction4"]:checked').val(), 
		  distance: parseInt($('[name="distance"]').val()), 
			times: parseInt($('[name="times"]').val())};
	// size
	} else if (effectType == "size") {
		effectOptions = {
			from: {width: parseInt($('[name="fromWidth"]').val()), height: parseInt($('[name="fromHeight"]').val())}, 
			to: {width: parseInt($('[name="toWidth"]').val()), height: parseInt($('[name="toHeight"]').val())}, 
			scale: $('[name="scale"]:checked').val()};
	// slide
	} else if (effectType == "slide") {
		effectOptions = {
			direction: $('[name="direction4"]:checked').val(),
			mode: $('[name="mode"]:checked').val(), 
		  distance: parseInt($('[name="distance"]').val())}; 
	}
	// 顯示動畫效果的特定選項
	displayEffectOptions();
}
//////////////////////////////////////////////
// 顯示動畫效果的特定選項
//////////////////////////////////////////////
function displayEffectOptions() {
	// 選取元素的CSS類別名稱
	var elementCSS = '.' + effectType;
	// 顯示指定CSS類別名稱的元素
	$(elementCSS).addClass('show');
}
//////////////////////////////////////////////
// 顯示動畫效果的指令
//////////////////////////////////////////////
function displayEffectCommand() {
	$('#effectCommand').html("$('#TestElement').effect('" + effectType + 
		"', " + displayData(effectOptions) + ", " + displayData(effectSpeed) + ");");
}