(function($){
  $.fn.setCSS = function(options) {  
    var settings = $.extend({
			text : '40', 
			css  : {
				float            : 'left', 
				border         	 : '1px solid red',
				color         	 : 'red',
				backgroundColor  : 'yellow', 
				width            : '40', 
				height           : '20', 
				margin           : '5', 
				textAlign        : 'center'				
			}
		}, options||{});
		
    return this.each(function() { 			
			$(this).css(settings.css).html(settings.text);
    });
  };
})(jQuery);