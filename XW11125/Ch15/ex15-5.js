(function($){
  var methods = {
		// 初始化div區塊
    init : function(options) {  
			var settings = $.extend({
				css  : {
					float            : 'left', 
					border         	 : '1px solid red',
					color         	 : 'red',
					backgroundColor  : 'yellow', 
					width            : '100', 
					height           : '100', 
					margin           : '5', 
					textAlign        : 'center', 
					padding          : '5'	
				}
			}, options||{});
			
			return this.each(function() { 			
				$(this).css(settings.css);
			});
		}, 
		// 在div區塊內顯示圖片
    show : function(options) {  		
			return this.each(function() {
				// 在div元素內加入檔案名稱為imageName的img元素
				if (options.image != null) {
					var image = $('<img src="' + options.image + '" />');
					$(this).css({'width': options.width, 
					  'height': options.height}).html(image);
				}
			});	  
		}, 
		// 在div區塊內隱藏圖片
    hide : function() {
			// 移除div元素的所有子元素
			return this.each(function() {
				$(this).empty();
			});	  
		}
  };

  $.fn.photoViewer = function(method) { 
    // 呼叫外掛程式的方法
    if (methods[method]) {
			// 第1個參數是方法的名稱, 第2個參數是方法的參數
      return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		// 呼叫init方法
    } else if (typeof method === 'object' || !method) {			
      return methods.init.apply(this, arguments);
    } else {
      $.error('您要呼叫的方法 : ' +  method + ' 不存在於photoViewer中');
    }  
  };
})(jQuery);