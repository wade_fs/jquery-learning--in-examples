(function($) {
  $.fn.maxHeight = function() {
    var max = 0;
    this.each(function() {
      max = Math.max(max, $(this).height());
    });
    return max;
  };
  $.fn.minHeight = function() {
    var min = 100000;
    this.each(function() {
      min = Math.min(min, $(this).height());
    });
    return min;
  };
})(jQuery);